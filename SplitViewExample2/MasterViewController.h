//
//  MasterViewController.h
//  SplitViewExample2
//
//  Created by Roger Whitney on 11/10/11.
//  Copyright (c) 2011 San Diego State University. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UIViewController

@property (strong, nonatomic) DetailViewController *detailViewController;
- (IBAction)red;
- (IBAction)blue;

@end
