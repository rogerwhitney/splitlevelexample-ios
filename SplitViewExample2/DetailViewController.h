//
//  DetailViewController.h
//  SplitViewExample2
//
//  Created by Roger Whitney on 11/10/11.
//  Copyright (c) 2011 San Diego State University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *color;
-(void) goRed;
-(void) goBlue;
@end
