//
//  SplitViewExample2Tests.m
//  SplitViewExample2Tests
//
//  Created by Roger Whitney on 11/10/11.
//  Copyright (c) 2011 San Diego State University. All rights reserved.
//

#import "SplitViewExample2Tests.h"

@implementation SplitViewExample2Tests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in SplitViewExample2Tests");
}

@end
